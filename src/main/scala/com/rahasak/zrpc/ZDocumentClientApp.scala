package com.rahasak.zrpc

import com.rahasak.proto.document.ZioDocument.DocumentServiceClient
import com.rahasak.proto.document.{DocumentCreateMessage, DocumentGetMessage}
import io.grpc.ManagedChannelBuilder
import scalapb.zio_grpc.ZManagedChannel
import zio.Layer
import zio.console._
import zio.stream.ZStream

object ZDocumentClientApp extends zio.App {

  val clientLayer: Layer[Throwable, DocumentServiceClient] =
    DocumentServiceClient.live(
      ZManagedChannel(
        ManagedChannelBuilder.forAddress("localhost", 9000).usePlaintext()
      )
    )

  def createDocument() = {
    val request = DocumentCreateMessage("1", "blob1")
    println(s"[unary] create document request $request")

    for {
      reply <- DocumentServiceClient.createDocument(request)
      _ <- putStrLn(s"[unary] create document reply $reply")
    } yield reply
  }

  def createDocuments() = {
    val documents = List(
      ("1", "blob1"),
      ("2", "blob2"),
      ("3", "blob4"),
      ("4", "blob1")
    )

    for {
      reply <- DocumentServiceClient.createDocuments(
        ZStream.fromIterable(
          documents.map { d =>
            val request = DocumentCreateMessage(d._1, d._2)
            println(s"[client stream] create documents request $request")

            request
          }
        )
      )
      _ <- putStrLn(s"[client stream] create documents reply $reply")
    } yield reply
  }

  def getDocuments() = {
    val request = DocumentGetMessage("1, 2, 3")
    println(s"[server stream] get documents request $request")

    val replyStream = DocumentServiceClient.getDocuments(request)
    replyStream.foreach(r => putStrLn(s"[server stream] get documents reply $r"))
  }

  def streamDocuments() = {
    val documents = List(
      ("1", "blob1"),
      ("2", "blob2"),
      ("3", "blob4"),
      ("4", "blob1")
    )

    val replyStream = for {
      reply <- DocumentServiceClient.streamDocuments(
        ZStream.fromIterable(
          documents.map(d => DocumentGetMessage(d._1))
        ).tap { r =>
          putStrLn(s"[bi-stream] document request $r")
        }
      )
    } yield reply

    replyStream.foreach(r => putStrLn(s"[bi-stream] document reply $r"))
  }

  override def run(args: List[String]) = {
    //createDocument().provideCustomLayer(clientLayer).exitCode
    //createDocuments().provideCustomLayer(clientLayer).exitCode
    //getDocuments().provideCustomLayer(clientLayer).exitCode
    streamDocuments().provideCustomLayer(clientLayer).exitCode
  }

}
