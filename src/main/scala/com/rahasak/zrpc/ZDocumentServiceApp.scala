package com.rahasak.zrpc

import com.rahasak.proto.document.ZioDocument.ZDocumentService
import com.rahasak.proto.document.{DocumentCreateMessage, DocumentGetMessage, DocumentReplyMessage, StatusReplyMessage}
import io.grpc.Status
import zio.console._
import zio.stream.ZStream
import zio.{ZEnv, ZIO, stream}

class ZDocumentServiceImpl extends ZDocumentService[ZEnv, Any] {
  override def createDocument(request: DocumentCreateMessage) = {
    println(s"[unary] create document request $request")

    val reply = StatusReplyMessage("201", "created document")
    println(s"[unary] create document reply $reply")

    ZIO.succeed(reply)
  }

  override def createDocuments(request: stream.Stream[Status, DocumentCreateMessage]) = {
    request.foreach(r => putStrLn(s"[client stream] create documents request $r"))

    val reply = StatusReplyMessage("201", "created document")
    println(s"[client stream] create documents reply $reply")

    ZIO.succeed(reply)
  }

  override def getDocuments(request: DocumentGetMessage) = {
    println(s"[server stream] get documents request $request")

    val documents = List(
      ("1", "blob1"),
      ("2", "blob2"),
      ("3", "blob4"),
      ("4", "blob1")
    )

    val replyStream = ZStream.fromIterable(
      documents.map(d => DocumentReplyMessage(d._1, d._2))
    ).tap { r =>
      putStrLn(s"[server-stream] document request $r")
    }

    replyStream
  }

  override def streamDocuments(request: stream.Stream[Status, DocumentGetMessage]) = {
    request.foreach(r => putStrLn(s"[bi-stream] document request $r"))

    val documents = List(
      ("1", "blob1"),
      ("2", "blob2"),
      ("3", "blob4"),
      ("4", "blob1")
    )

    val replyStream = ZStream.fromIterable(
      documents.map(d => DocumentReplyMessage(d._1, d._2))
    ).tap { r =>
      putStrLn(s"[server-stream] document request $r")
    }

    replyStream
  }
}

import scalapb.zio_grpc.{ServerMain, ServiceList}

object ZDocumentServiceApp extends ServerMain {
  override def port: Int = 9000

  def services: ServiceList[zio.ZEnv] = ServiceList.add(new ZDocumentServiceImpl)

  //val createRouteGuide = for {
  //  _ <- Ref.make(Map.empty[String, List[String]])
  //} yield new ZioDocumentServiceImpl()
  //def services: ServiceList[zio.ZEnv] = ServiceList.addM(createRouteGuide)
}
