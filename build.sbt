name := "zrpc"

version := "1.0"

scalaVersion := "2.13.4"

val grpcVersion = "1.41.0"

libraryDependencies ++= {
  Seq(
    "io.grpc"                 % "grpc-netty"                % grpcVersion,
    "com.thesamet.scalapb"    %% "scalapb-runtime-grpc"     % scalapb.compiler.Version.scalapbVersion
  )
}

// scalapb code generatory and zio-grpc code generator
PB.targets in Compile := Seq(
  scalapb.gen() -> (sourceManaged in Compile).value,
  scalapb.zio_grpc.ZioCodeGenerator -> (sourceManaged in Compile).value / "scalapb"
)

// define merge conflict strategy to netty 
assemblyMergeStrategy in assembly := {
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case "module-info.class" => MergeStrategy.discard
  case x if x.contains("io.netty.versions.properties") => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}
